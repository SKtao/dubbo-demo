package com.ktao.consumer.controller;

import com.ktao.api.api.DemoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: kongtao
 * @Date: 2019-03-26 14:05
 * @Description:
 */
@RestController
@RequestMapping("/dubbo")
public class TestController {

    @Resource
    private DemoService service;

    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public void say() throws Exception {
            service.sayHello("kongtao");
    }
}
