package com.ktao.api.api;

/**
 * @Author: kongtao
 * @Date: 2019-03-26 13:56
 * @Description:
 */
public interface DemoService {

    String sayHello(String str)throws Exception;
}
