package com.ktao.provider.service;

import com.alibaba.dubbo.rpc.RpcException;
import com.ktao.api.api.DemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: kongtao
 * @Date: 2019-03-26 13:57
 * @Description:
 */
public class DemoServiceImpl implements DemoService {

    private static Logger log = LoggerFactory.getLogger(DemoServiceImpl.class);

    @Override
    public String sayHello(String str) {
        return "hello "+str;
    }
}
