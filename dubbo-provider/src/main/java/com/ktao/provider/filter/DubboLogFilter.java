package com.ktao.provider.filter;

import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dubbo日志Filter
 * @Author: kongtao
 * @Date: 2019-03-25 12:37
 * @Description:
 */
@Activate
public class DubboLogFilter implements Filter {

    private static Logger log = LoggerFactory.getLogger(DubboLogFilter.class);

    public DubboLogFilter(){

    }
    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        if (invoker.getUrl() != null) {
            log.info("开始调用接口{}.{}，参数：{}", invoker.getInterface().getSimpleName(),invocation.getMethodName(), invocation.getArguments());
        }
        Result result = null;
        long start =0L;
        try{
            //接口调用开始时间
            start = System.currentTimeMillis();
            result = invoker.invoke(invocation);
            if (result.hasException()){
                log.warn("调用接口{}.{}异常!",invoker.getInterface().getSimpleName(),invocation.getMethodName(),result.getException());
            }
        }catch (Throwable e){
            if (e instanceof RuntimeException){
                throw (RuntimeException)e;
            }
            throw new RpcException("调用DubboLogFilter产生未知异常: {}",e);
        }finally {
            //计算接口调用时间
            long elapsed = System.currentTimeMillis() - start;
            log.info("接口{}.{}调用结束，响应时间：{}", invoker.getInterface().getSimpleName(),invocation.getMethodName(), elapsed);
        }
        return result;

    }
}
